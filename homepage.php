<?php /* Template Name: Salpa-Homepage */ ?>
<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package salpa
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<div id="content" class="site-content bg-white">

		<div class="position-relative overflow-hidden mx-auto d-flex flex-column text-center bg-dark salpa-home-cover">
			<video class="position-absolute w-100 salpa-home-cover-video-bg" playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
					<source src="<?php echo get_stylesheet_directory_uri() . '/assets/background-video.mp4' ; ?>" type="video/mp4">
			</video>

			<main role="main" class="my-auto text-white col-md-7 p-5 mx-auto salpa-home-cover-content">
				<h1 class="cover-heading">سالپا؛ مرجع مصاحبه‌های شغلی</h1>
				<p class="lead">
						الان اینجا فقط یک متنی به صورت آزمایشی می‌خواهیم بنویسم تا بعدا در مورد اینکه این متن چه باشد، تصمیم بگیریم.
						<i class="fas fa-heart text-danger"></i>
				</p>

			
				<div class="input-group mb-3 mt-5">
					<input 
						type="text" 
						class="form-control pl-3 form-control-lg" 
						placeholder="نام شرکت مورد نظر را جستجو کنید مثلا شرکت سالپا"
						aria-label="Recipient's username"
						aria-describedby="button-addon2"
					>
					<div class="input-group-append">
						<button class="btn btn-md btn-danger m-0 px-3 py-2 z-depth-2 waves-effect" type="button">
							<i class="fas fa-search"></i>
						</button>
					</div>
				</div>

			</main>
		</div>


		<section class="pb-5">

			<div class="container">
				<div class="col-md-10 pt-5 pb-5 mt-5 mb-5 mx-auto">
					<div class="row home-companies">
						
					<!--Loop-->
						<div class="col-4 col-md-2 company-item">
							<a href="" class="overflow-hidden d-inline-block p-2 border border-light border rounded-circle">
								<img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/logos/digikala.png" alt="<-AltImage->" class="rounded-circle">
							</a>	
						</div>
					<!--/Loop-->
						<div class="col-4 col-md-2">
							<a href="" class="overflow-hidden d-inline-block p-2 border border-light border rounded-circle company-item">
								<img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/logos/digikala.png" alt="<-AltImage->" class="rounded-circle">
							</a>	
						</div>
						<div class="col-4 col-md-2">
							<a href="" class="overflow-hidden d-inline-block p-2 border border-light border rounded-circle company-item">
								<img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/logos/digikala.png" alt="<-AltImage->" class="rounded-circle">
							</a>	
						</div>
						<div class="col-4 col-md-2">
							<a href="" class="overflow-hidden d-inline-block p-2 border border-light border rounded-circle company-item">
								<img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/logos/digikala.png" alt="<-AltImage->" class="rounded-circle">
							</a>	
						</div>
						<div class="col-4 col-md-2">
							<a href="" class="overflow-hidden d-inline-block p-2 border border-light border rounded-circle company-item">
								<img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/logos/digikala.png" alt="<-AltImage->" class="rounded-circle">
							</a>	
						</div>
						<div class="col-4 col-md-2">
							<a href="#" class="position-relative d-inline-block mx-auto my-auto h-100 w-100 p-0 border border-light border rounded-circle company-item more-button  btn btn-outline-info waves-effect">
								<span class="position-absolute pr-3 pl-3 h-100 d-flex align-items-center">همه شرکت‌ها رو ببینم ...</span>
							</a>	
						</div>
					</div>
				</div>
			</div>
		</section>


		<section>
			<div class="container">
				<div class="col-md-11 pt-5 pb-5 mt-5 mb-5 mx-auto">
					<div class="row home-interviews grid-divider">
						<!--Loop-->
						<div class="col-md-6 pt-4 pb-4">
								<div class="media position-relative home-interview-item">
								<img src="http://localhost/salpa/wp-content/themes/salpa/assets/logos/digikala.png" class="align-self-start mr-4" alt="...">
									<div class="media-body">
										<h5 class="mt-0">مصاحبه مدیر مسئول</h5>
										<p>
										در مرحله اول با پرکرد اپلیکیشن فرم و یک گفتگو با کارشناس HR شروع شد. هم سوال‌هایی در مورد سابقه کاری من پرسیده شد هم یک توضیح ....
										</p>
										<a href="یبsdfsdfشsdfsdfسیب" class="stretched-link float-right">
											مشاهده تجربه
											<i class="fas fa-angle-left ml-1 align-middle"></i>
										</a>
									</div>
								</div>
							</div>
						<!--/Loop-->
						

						
						<div class="col-md-6 pt-4 pb-4">
								<div class="media position-relative home-interview-item">
								<img src="http://localhost/salpa/wp-content/themes/salpa/assets/logos/digikala.png" class="align-self-start mr-4" alt="...">
									<div class="media-body">
										<h5 class="mt-0">مصاحبه مدیر مسئول</h5>
										<p>
										در مرحله اول با پرکرد اپلیکیشن فرم و یک گفتگو با کارشناس HR شروع شد. هم سوال‌هایی در مورد سابقه کاری من پرسیده شد هم یک توضیح ....
										</p>
										<a href="یبsdfsdfشsdfsdfسیب" class="stretched-link float-right">
											مشاهده تجربه
											<i class="fas fa-angle-left ml-1 align-middle"></i>
										</a>
									</div>
								</div>
							</div>

							<div class="col-md-6 pt-4 pb-4">
								<div class="media position-relative home-interview-item">
								<img src="http://localhost/salpa/wp-content/themes/salpa/assets/logos/digikala.png" class="align-self-start mr-4" alt="...">
									<div class="media-body">
										<h5 class="mt-0">مصاحبه مدیر مسئول</h5>
										<p>
										در مرحله اول با پرکرد اپلیکیشن فرم و یک گفتگو با کارشناس HR شروع شد. هم سوال‌هایی در مورد سابقه کاری من پرسیده شد هم یک توضیح ....
										</p>
										<a href="یبsdfsdfشsdfsdfسیب" class="stretched-link float-right">
											مشاهده تجربه
											<i class="fas fa-angle-left ml-1 align-middle"></i>
										</a>
									</div>
								</div>
							</div>

							<div class="col-md-6 pt-4 pb-4">
								<div class="media position-relative home-interview-item">
								<img src="http://localhost/salpa/wp-content/themes/salpa/assets/logos/digikala.png" class="align-self-start mr-4" alt="...">
									<div class="media-body">
										<h5 class="mt-0">مصاحبه مدیر مسئول</h5>
										<p>
										در مرحله اول با پرکرد اپلیکیشن فرم و یک گفتگو با کارشناس HR شروع شد. هم سوال‌هایی در مورد سابقه کاری من پرسیده شد هم یک توضیح ....
										</p>
										<a href="یبsdfsdfشssdfsdfdfsdfسیب" class="stretched-link float-right">
											مشاهده تجربه
											<i class="fas fa-angle-left ml-1 align-middle"></i>
										</a>
									</div>
								</div>
							</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="your-interviews pb-5 pt-5">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-md-6 text-center text-white">
						<span class="d-block py-3 text-2rem">
						شما هم اگر تجربه مصاحبه داری میتونی با دوستات به اشتراک بذاری:
						</span>
					</div>
					<div class="col-md-6 text-center">
						<button type="button" class="btn btn-outline-white btn-rounded btn-lg btn-my-experience my-4">تجربه مصاحبه‌ی من</button>
					</div>
				</div>
			</div>
		</section>

		<section class="pt-5 pb-5">
			<div class="container">
				<div class="row align-items-center m-3 p-4 p-md-5 home-email-subscription">
					<div class="col-md-6 text-center">
						<span class="lead">
							اینجا ما یک چک لیستی چیزی آماده کردیم که توی مصاحبه کمکت می‌کنه اگر ایمیلت رو بدی برات ایمیل می‌کنیم :)
						</span>
					</div>
					<div class="col-md-6 text-center">
						<form class="text-center px-md-5 py-5">
							<input type="email" id="" class="form-control form-control-lg px-3" placeholder="ایمیل قشنگتون؟">
							<button class="btn btn-rounded btn-danger btn-block my-4" type="submit">دریافت چک لیست</button>
						</form>
					</div>
				</div>
			</div>
		</section>
		
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) :
				?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
				<?php
			endif;

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
