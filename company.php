<?php /* Template Name: Company */ ?>
<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package salpa
 */

get_header();
?>


	<div id="primary" class="content-area">
		
		<main id="main" class="site-main">
			<div class="bg-light bg-cover-companies"></div>
			<div class="bg-white">
				<div class="container py-3 position-relative">
					<div class="d-flex">
						<div class="company-logo">
							<img class="img-fluid rounded-circle" src="http://localhost/salpa/wp-content/themes/salpa/assets/logos/digikala.png" />
						</div>
						<h2 class="d-inline-block p-3 text-bold company-page-name">
							دیجی کالا
							<span class="valid-badge"></span>
						</h2>
					</div>
				</div>
				<div class="company-page-tabs mt-2">
					<div class="container">
						<ul class="nav nav-pills">
							<li class="nav-item">
								<a class="nav-link active" href="#">درباره‌ی سالپا چی میدونی؟</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">مصاحبه‌های سالپا</a>
							</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="row py-5">
					<div class="col-md-8 col-lg-9 pr-5">
						<div class="card w-100 mb-4 px-5">
							<div class="row py-4">
								<div class="col-md-4">
									<div class="min-height-9rem">
										<canvas id="interviewerBehavior"></canvas>
									</div>	
									<div class="py-3">
										<span class="text-1-2rem text-center">برخورد مصاحبه‌کننده‌ها</span>
										<ul class="list-unstyled mt-3 pl-2 text-muted">
											<li>
												<i class="fa fa-circle fa-xs text-salpa-blue"></i>
												<span class="ml-2">خیلی خوب بود</span>
											</li>
											<li>
												<i class="fa fa-circle fa-xs text-salpa-green"></i>
												<span class="ml-2">خوب بود</span>
											</li>
											<li>
												<i class="fa fa-circle fa-xs text-salpa-yellow"></i>
												<span class="ml-2">عادی بود</span>
											</li>
											<li>
												<i class="fa fa-circle fa-xs text-salpa-violet"></i>
												<span class="ml-2">مناسب نبود</span>
											</li>
											<li>
												<i class="fa fa-circle fa-xs text-salpa-red"></i>
												<span class="ml-2">خیلی بد بود</span>
											</li>
										<ul>
									</div>
								</div>
								<div class="col-md-4">
									<div class="min-height-9rem">
										<canvas id="interviewFeel"></canvas>
									</div>
									<div class="py-3">
										<span class="text-1-2rem text-center">حس نسبت به مصاحبه</span>
										<ul class="list-unstyled mt-3 pl-2 text-muted">
											<li>
												<i class="fa fa-circle fa-xs text-salpa-yellow"></i>
												<span class="ml-2">نه خوب، نه بد</span>
											</li>
											<li>
												<i class="fa fa-circle fa-xs text-salpa-green"></i>
												<span class="ml-2">خوب</span>
											</li>
											<li>
												<i class="fa fa-circle fa-xs text-salpa-red"></i>
												<span class="ml-2">بد</span>
											</li>
										<ul>
									</div>
								</div>
								<div class="col-md-4">
									<div class="min-height-9rem d-flex justify-content-center align-items-center">
										<div class="interview-hardness-rate">۴.۱</div>
									</div>
									<div class="py-3">
										<span class="text-1-2rem text-center">سطح دشواری مصاحبه</span>
									</div>
								</div>
							</div>
						</div>
						<div class="card w-100 mb-4">
							<div class="card-body p-5">
								<div class="d-flex justify-content-between align-items-end mb-4">
									<h3 class="text-salpa-blue-ocean text-bold text-2rem">درباره کافه بازار</h3>
									<div class="text-0-8rem">
										<a href="" class="text-muted px-1"><i class="fab fa-instagram fa-2x"></i></a>
										<a href="" class="text-muted px-1"><i class="fab fa-linkedin fa-2x"></i></a>
										<a href="" class="text-muted px-1"><i class="fab fa-pinterest fa-2x"></i></a>
										<a href="" class="text-muted px-1"><i class="fab fa-twitter fa-2x"></i></a>
									</div>
								</div>
								<p class="card-text">
								برنامه‌ی «بازار» یک بستر ارائه‌ی برنامه‌ها و بازی‌های اندرویدی در ایران است که از سوی شرکت آوای همراه هوشمند هزاردستان توسعه یافته و در مالکیت «گروه توسعه فناوری اطلاعات هزاردستان» قرار دارد.

کافه‌بازار در بهمن ۱۳۸۹ به همت جمعی از فارغ‌التحصیلان و دانشجویان دانشگاه‌های ایرانی شکل گرفت و از آن زمان تاکنون، با بهره‌مندی از همکاری متخصصان و توسعه‌دهندگان مشتاق و مستعد ایرانی، کوشیده است بستر جامعی از خدمات و  محصولات دیجیتال را برای کاربران اندرویدی فراهم کند.

								</p>
							</div>
						</div>

						<div class="card w-100 mb-4">
							<div class="card-body p-5">
								<div class="row text-center mb-4">
									<div class="col-sm">
										<strong class="d-block mb-2">شبکه‌های اجتماعی کافه بازار</strong>
										<div class="text-1rem">
											<a href="" class="text-muted px-1"><i class="fab fa-instagram fa-2x"></i></a>
											<a href="" class="text-muted px-1"><i class="fab fa-linkedin fa-2x"></i></a>
											<a href="" class="text-muted px-1"><i class="fab fa-pinterest fa-2x"></i></a>
											<a href="" class="text-muted px-1"><i class="fab fa-twitter fa-2x"></i></a>
										</div>
									</div>
									<div class="col-sm">
										<strong class="d-block mb-2">مدیر منابع انسانی کافه بازار</strong>
										<span class="text-info text-bold text-1-2rem">میلاد بافراست</span>
									</div>
									<div class="col-sm">
										<strong class="d-block mb-2">اندازه شرکت کافه بازار</strong>
										<span class="text-muted text-bold text-1-2rem">۳۰۰ تا ۵۰۰ نفر</span>
									</div>
								</div>
								<div class="row text-center">
									<div class="col-sm">
										<strong class="d-block mb-2">ساعت کاری کافه بازار</strong>
										<span class="text-muted text-bold text-1-2rem">منعطف</span>
									</div>
									<div class="col-sm">
										<strong class="d-block mb-2">مدیرعامل کافه بازار</strong>
										<span class="text-info text-bold text-1-2rem">حسام آرمندهی</span>
									</div>
									<div class="col-sm">
										<strong class="d-block mb-2">نوع صنعت</strong>
										<span class="text-muted text-bold text-1-2rem">مالی، بانکی و بیمه</span>
									</div>
								</div>
							</div>
						</div>

						<div class="card w-100 mb-4">
							<div class="card-body py-4 px-5">
								<p class="card-text">
									<strong class="mr-4 text-1rem">زمینه فعالیت کافه بازار</strong>
									<span class="text-1rem">فروشگاه اینترنی، مالی، بانکی و ...</span>
								</p>
							</div>
						</div>


						<div class="card w-100 mb-4">
							<div class="card-body px-5 py-4">
								<?php require get_template_directory() . '/components/experience-card-body.php';?>
							</div>
						</div>


						<div class="card w-100 mb-4">
							<div class="card-body px-5 py-4 dashed-divider">
								<?php	for ($cards = 0; $cards <= 3; $cards++) { ?>
								<div class="dashed-divider-item my-2 py-3">
									<?php require get_template_directory() . '/components/experience-card-body.php';?>
								</div>
								<?php	} ?>
							</div>
						</div>
					</div>



					<div class="col-md-4 col-lg-3 d-none d-xl-block d-lg-block d-md-block">
						<a class="btn btn-primary btn-lg d-block mx-0 mb-5" href="" title="" role="button">تجربه مصاحبه من</a>

						<div class="card">
							<div class="card-header text-bold text-center text-sp-blue p-3 text-1-2rem">
								آوای هوشمند هزاردستان
							</div>
							<ul class="list-group w-100 text-center">
								<li class="list-group-item">
									<b class="d-block">سایت شرکت کافه بازار</b>
									<a href="https://www.cafebazaar.ir/" title="سایت شرکت کافه بازار">www.cafebazaar.ir</a>
								</li>

								<li class="list-group-item">
									<b class="d-block">تلفن تماس کافه بازار</b>
									<span class="text-muted">۰۲۱-۷۷۸۸۹۹۶۶</span>	
								</li>

								<li class="list-group-item">
									<b class="d-block">ایمیل کافه بازار</b>
									<a href="mailto:info@cafebazaar.ir" title="ایمیل کافه بازار">info@cafebazaar.ir</a>
								</li>

								<li class="list-group-item">
									<b class="d-block">آدرس شرکت کافه بازار</b>
									<span class="text-muted">
										تهران، سعادت آباد، ضلع جنوب شرقی میدان فرهنگ، پیوند ۲، پلاک ۲
									</span>
								</li>

								<li class="list-group-item p-0">
									<!--commented map for not loading --->
									<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3200.8539190983693!2d51.383958514608345!3d35.78265943201135!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f8e07ca5ed345b1%3A0x48a70aebd941693f!2sCafe+Bazaar!5e0!3m2!1sen!2sus!4v1555846500395!5m2!1sen!2sus" frameborder="0" style="border:0" allowfullscreen></iframe>
								 -->
								</li>
								
							</ul>
						</div>

						<div class="card mt-5 px-3">
								
								<div class="card-body">

									<span class="text-bold text-center text-1-2rem text-salpa-blue-ocean py-3 d-block card-title">
										شرکت‌های مشابه
									</span>

									<div class="row">
										<a href="#" class="col-6 p-2 flex-column list-group-item-action text-center">
											<img src="http://localhost/salpa/wp-content/themes/salpa/assets/logos/digikala.png" 
													class="max-width-5rem rounded-circle" alt="...">
											<span class="d-block mt-1">شرکت رهنما</span>
										</a>

										<a href="#" class="col-6 p-2 flex-column list-group-item-action text-center">
											<img src="http://localhost/salpa/wp-content/themes/salpa/assets/logos/digikala.png" 
													class="max-width-5rem rounded-circle" alt="...">
											<span class="d-block mt-1">شرکت رهنما</span>
										</a>

										<a href="#" class="col-6 p-2 flex-column list-group-item-action text-center">
											<img src="http://localhost/salpa/wp-content/themes/salpa/assets/logos/digikala.png" 
													class="max-width-5rem rounded-circle" alt="...">
											<span class="d-block mt-1">شرکت رهنما</span>
										</a>

										<a href="#" class="col-6 p-2 flex-column list-group-item-action text-center">
											<img src="http://localhost/salpa/wp-content/themes/salpa/assets/logos/digikala.png" 
													class="max-width-5rem rounded-circle" alt="...">
											<span class="d-block mt-1">شرکت رهنما</span>
										</a>

	
									</div>
								</div>
						</div>
				
					</div>
				</div>
			</div>
		</main>
	</div>

<?php
get_footer();
