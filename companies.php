<?php /* Template Name: Companies */ ?>
<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package salpa
 */

get_header();
?>


	<div id="primary" class="content-area">
		
		<main id="main" class="site-main">
			<div class="bg-light bg-cover-companies"></div>
			<div class="bg-sp-blue-gradient">
				<div class="container text-white py-3">
					<h2 class="text-2rem"> <!--Should check prority-->
						همه شرکت‌ها
					</h2>
				</div>
			</div>
			<div class="bg-white">
				<div class="container">
					<div class="row py-5">
						<div class="col-md-8 col-lg-9 pr-5">
							
							<div class="input-group">
								<input 
									type="text" 
									class="form-control pl-5 form-control-lg text-1rem salpa-company-search-bar" 
									placeholder="نام شرکت مورد نظر را جستجو کنید مثلا شرکت سالپا"
									aria-label="نام شرکت"
									aria-describedby="button-addon2"
								>
								<div class="input-group-append">
									<button class="btn btn-md btn-danger m-0 px-4 py-2 z-depth-2 waves-effect" type="button">
										<i class="fas fa-2x fa-search"></i>
									</button>
								</div>
							</div>
							<div class="row home-interviews grid-divider py-5">
								<!--Loop-->
								<div class="col-md-12 col-sm-12 col-lg-6 pt-4 pb-4">
									<div class="media position-relative home-interview-item">
									<img src="http://localhost/salpa/wp-content/themes/salpa/assets/logos/digikala.png" class="align-self-start mr-4" alt="...">
										<div class="media-body">
											<h5 class="mt-0">
												<span class="text-bold">گروه صنعتی گلرنگ</span>
												<span class="valid-badge"></span>
											</h5>
											<p>
												<div class="d-flex align-items-center">
													<div class="text-warning mr-3" style="font-size:1.5rem;">
														<i class="far fa-star"></i>	
														<i class="fas fa-star-half-alt"></i>
														<i class="fas fa-star"></i>	
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
													</div>
													<div class="text-muted">
														۱۲۰ تجربه
													</div>	
												</div>
											</p>
											<a href="یبsdfsdfشsdfsdfسیب" class="stretched-link float-right">
												مشاهده شرکت
												<i class="fas fa-angle-left ml-1 align-middle"></i>
											</a>
										</div>
									</div>
								</div>
								<!--/Loop-->
								<div class="col-md-12 col-sm-12 col-lg-6 pt-4 pb-4">
									<div class="media position-relative home-interview-item">
									<img src="http://localhost/salpa/wp-content/themes/salpa/assets/logos/digikala.png" class="align-self-start mr-4" alt="...">
										<div class="media-body">
											<h5 class="mt-0">
												<span class="text-bold">گروه صنعتی گلرنگ</span>
												<span class="valid-badge"></span>
											</h5>
											<p>
												<div class="d-flex align-items-center">
													<div class="text-warning mr-3" style="font-size:1.5rem;">
														<i class="far fa-star"></i>	
														<i class="fas fa-star-half-alt"></i>
														<i class="fas fa-star"></i>	
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
													</div>
													<div class="text-muted">
														۱۲۰ تجربه
													</div>	
												</div>
											</p>
											<a href="یبsdfsdfشsdfsdfسیب" class="stretched-link float-right">
												مشاهده شرکت
												<i class="fas fa-angle-left ml-1 align-middle"></i>
											</a>
										</div>
									</div>
								</div>

								<div class="col-md-12 col-sm-12 col-lg-6 pt-4 pb-4">
									<div class="media position-relative home-interview-item">
									<img src="http://localhost/salpa/wp-content/themes/salpa/assets/logos/digikala.png" class="align-self-start mr-4" alt="...">
										<div class="media-body">
											<h5 class="mt-0">
												<span class="text-bold">گروه صنعتی گلرنگ</span>
												<span class="valid-badge"></span>
											</h5>
											<p>
												<div class="d-flex align-items-center">
													<div class="text-warning mr-3" style="font-size:1.5rem;">
														<i class="far fa-star"></i>	
														<i class="fas fa-star-half-alt"></i>
														<i class="fas fa-star"></i>	
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
													</div>
													<div class="text-muted">
														۱۲۰ تجربه
													</div>	
												</div>
											</p>
											<a href="یبsdfsdfشsdfsdfسیب" class="stretched-link float-right">
												مشاهده شرکت
												<i class="fas fa-angle-left ml-1 align-middle"></i>
											</a>
										</div>
									</div>
								</div>

								<div class="col-md-12 col-sm-12 col-lg-6 pt-4 pb-4">
									<div class="media position-relative home-interview-item">
									<img src="http://localhost/salpa/wp-content/themes/salpa/assets/logos/digikala.png" class="align-self-start mr-4" alt="...">
										<div class="media-body">
											<h5 class="mt-0">
												<span class="text-bold">گروه صنعتی گلرنگ</span>
												<span class="valid-badge"></span>
											</h5>
											<p>
												<div class="d-flex align-items-center">
													<div class="text-warning mr-3 text-1-5rem">
														<i class="far fa-star"></i>	
														<i class="fas fa-star-half-alt"></i>
														<i class="fas fa-star"></i>	
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
													</div>
													<div class="text-muted">
														۱۲۰ تجربه
													</div>	
												</div>
											</p>
											<a href="یبsdfsdfشsdfsdfسیب" class="stretched-link float-right">
												مشاهده شرکت
												<i class="fas fa-angle-left ml-1 align-middle"></i>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-lg-3 d-none d-xl-block d-lg-block d-md-block">
							<a class="btn btn-primary btn-lg d-block mx-0 mb-5" href="" title="" role="button">تجربه مصاحبه من</a>

							<div class="card">
								<div class="card-header text-bold text-center text-sp-blue p-3 text-1-2rem">
									دسته بندی شرکت‌ها
								</div>
								<ul class="list-group w-100 text-center">
									<a href="#" class="list-group-item list-group-item-action">تجارت الکترونیک و خدمات آنلاین</a>
									<a href="#" class="list-group-item list-group-item-action">تجارت الکترونیک و خدمات آنلاین</a>
									<a href="#" class="list-group-item list-group-item-action">تجارت الکترونیک و خدمات آنلاین</a>
									<a href="#" class="list-group-item list-group-item-action">تجارت الکترونیک و خدمات آنلاین</a>
									<a href="#" class="list-group-item list-group-item-action">تجارت الکترونیک و خدمات آنلاین</a>
								</ul>
							</div>

							<div class="card card-floated-title mt-5 px-3">
								<div class="card-header-floated-title">
									<span>شرکت‌های برتر</span>
								</div>
								<div class="list-group list-group-flush">
									<a href="#" class="list-group-item list-group-item-action text-right d-flex align-items-center justify-content-between">
										<span>شرکت رهنما</span>
										<img src="http://localhost/salpa/wp-content/themes/salpa/assets/logos/digikala.png" 
												class="max-width-5rem" alt="...">
									</a>
									<a href="#" class="list-group-item list-group-item-action text-right d-flex align-items-center justify-content-between">
										<span>شرکت رهنما</span>
										<img src="http://localhost/salpa/wp-content/themes/salpa/assets/logos/digikala.png" 
												class="max-width-5rem" alt="...">
									</a>
									<a href="#" class="list-group-item list-group-item-action text-right d-flex align-items-center justify-content-between">
										<span>شرکت رهنما</span>
										<img src="http://localhost/salpa/wp-content/themes/salpa/assets/logos/digikala.png" 
												class="max-width-5rem" alt="...">
									</a>
								</div>
							</div>
					
						</div>
					</div>
				</div>
			</div>
		</main>
	</div>

<?php
get_footer();
