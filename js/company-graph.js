$(function(){
  var interviewerBehavior = document.getElementById("interviewerBehavior").getContext('2d');
  var interviewFeel = document.getElementById("interviewFeel").getContext('2d');

  var lineChart1 = new Chart(interviewerBehavior, {
    type: 'doughnut',
    data: {
      labels: ["خیلی خوب بود", "خوب بود", "عادی بود", "مناسب نبود", "خیلی بد بود"],
      datasets: [{
        data: [300, 50, 100, 40, 120],
        backgroundColor: ["#219ddb", "#00bf8c", "#ffb822", "#ad85cc", "#e12833"],
        hoverBackgroundColor: ["#4dacdb", "#26bf96", "#ffc954", "#bfadcc", "#e0555f"]
      }]
    },
    options: {
      responsive: true,
      animation: {
        easing: 'easeInOutQuint'
      },
      legend: {
        position: 'bottom',
        display: false,
        labels: {
        
          fontStyle: "bold",
          fontFamily: 'IRANSans, Tahoma',
          fontSize: 5,
          padding: 15
        }
      }
    }
  });

  var lineChart2 = new Chart(interviewFeel, {
    type: 'doughnut',
    data: {
      labels: ["نه خوب، نه بد", "خوب", "بد"],
      datasets: [{
        data: [300, 50, 100],
        backgroundColor: ["#00bf8c", "#ffb822", "#e12833"],
        hoverBackgroundColor: ["#26bf96", "#ffc954", "#e0555f"]
      }]
    },
    options: {
      responsive: true,
      animation: {
        easing: 'easeInOutQuint'
      },
      legend: {
        position: 'bottom',
        display: false,
        labels: {
        
          fontStyle: "bold",
          fontFamily: 'IRANSans, Tahoma',
          fontSize: 5,
          padding: 15
        }
      }
    }
  });

})