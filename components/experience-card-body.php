
  <div class="text-right text-muted">تاریخ تجربه: تیر ۱۳۹۷</div>

  <div class="media position-relative mb-4">
    <img src="http://localhost/salpa/wp-content/themes/salpa/assets/logos/digikala.png" class="align-self-start rounded-circle border max-width-8rem mr-4" alt="...">
    <div class="align-self-center media-body">
      <h5 class="mt-0 text-bold">مصاحبه کارشناس جذب و استخدام</h5>
      <h6 class="mt-0">Recruiter specialist</h6>
    </div>
  </div>

  <div class="">
    <div class="py-2">
      <div class="text-muted d-inline-block pr-4">
        حس <span class="text-success">خوب</span> از مصاحبه
      </div>
      <div class="text-muted d-inline-block pr-4">
        مصاحبه <span class="text-danger">سخت</span> بود
      </div>
      <div class="text-muted d-inline-block pr-4">
        مصاحبه <span class="text-info">۵</span> مرحله داشت
      </div>
    </div>

    <div class="py-2">
      <div class="text-muted d-inline-block pr-4">
        نحوه درخواست <span class="text-info">کارمندان شرکت</span>
      </div>
      <div class="text-muted d-inline-block pr-4">
        دعوت به مصاحبه <span class="text-info">یک الی دو هفته	</span>
      </div>
      <div class="text-muted d-inline-block pr-4">
        کل فرایند <span class="text-info">دو ماه</span>
      </div>
    </div>


    <div class="py-2">
      <div class="text-muted d-inline-block pr-4">
        برخورد مصاحبه کننده <span class="text-success">خیلی خوب</span>
      </div>
      <div class="text-muted d-inline-block pr-4">
        سابقه کار <span class="text-info">کمی مهم بود</span>
      </div>
      <div class="text-muted d-inline-block pr-4">
        <span class="text-info">پذیرفته شدم و قبول کردم</span>
      </div>
    </div>


    <div class="mt-5 d-block text-indent-1">
      <p>
      لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
      <a href="">بیشتر بخوانید</a>
      </p>
    </div>

    <div class="">
      <ul class="list-unstyled salpa-red-bulleted-list">
        <li class="mb-5">
          <span class="text-1-2rem text-bold pb-2 d-inline-block">توصیه به کسی که می‌خواد بره مصاحبه</span>
          <p>
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
          </p>
        </li>

        <li class="mb-5">
          <span class="text-1-2rem text-bold pb-2 d-inline-block">توصیه به کسی که می‌خواد بره مصاحبه</span>
          <p>
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
          </p>
        </li>


        <li class="mb-5">
          <span class="text-1-2rem text-bold pb-2 d-inline-block">توصیه به کسی که می‌خواد بره مصاحبه</span>
          <p>
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
          </p>
        </li>


        <li class="mb-5">
          <span class="text-1-2rem text-bold pb-2 d-inline-block">توصیه به کسی که می‌خواد بره مصاحبه</span>
          <p>
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
          </p>
        </li>
      </ul>

      <ul class="list-unstyled salpa-red-bulleted-list">
        <li class="mb-5">
          <span class="text-1-2rem text-bold pb-2 d-inline-block">توصیه به کسی که می‌خواد بره مصاحبه</span>
          <p>
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
          </p>
        </li>

        <li class="mb-5">
          <span class="text-1-2rem text-bold pb-2 d-inline-block">توصیه به کسی که می‌خواد بره مصاحبه</span>
          <p>
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
          </p>
        </li>


        <li class="mb-5">
          <span class="text-1-2rem text-bold pb-2 d-inline-block">توصیه به کسی که می‌خواد بره مصاحبه</span>
          <p>
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
          </p>
        </li>


        <li class="mb-5">
          <span class="text-1-2rem text-bold pb-2 d-inline-block">توصیه به کسی که می‌خواد بره مصاحبه</span>
          <p>
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
            الکی خودت رو خفن نشون بده.
          </p>
        </li>
      </ul>
      <ul class="list-unstyled salpa-red-bulleted-list row">
        <li class="mb-5 col-6">
          <span class="text-1-2rem text-bold pb-2 d-inline-block">حقوق پیشنهادی من</span>
          <p>
           ۲.۵ میلیون تومان
          </p>
        </li>

        <li class="mb-5 col-6">
          <span class="text-1-2rem text-bold pb-2 d-inline-block">حقوق پیشنهادی آنها</span>
          <p>
            ۲.۵ میلیون تومان
          </p>
        </li>
      </ul>
    </div>

    <div class="mt-5 d-flex justify-content-between align-items-center">
      <div class="text-left">
        <a href="#" class="p-3 text-muted d-inline-block text-bold text-1-2rem">
          <i class="far fa-flag"></i>

        </a>
        <button class="btn btn-outline-info text-bold">مفید بود (11)</button>
      </div>
      <div class="text-1rem">
        <a href="" class="text-muted px-1"><i class="fab fa-instagram fa-2x"></i></a>
        <a href="" class="text-muted px-1"><i class="fab fa-linkedin fa-2x"></i></a>
        <a href="" class="text-muted px-1"><i class="fab fa-pinterest fa-2x"></i></a>
        <a href="" class="text-muted px-1"><i class="fab fa-twitter fa-2x"></i></a>
      </div>
    </div>
  </div>

