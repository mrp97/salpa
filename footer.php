<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package salpa
 */

?>

	</div><!-- #content -->

	<footer class="bg-footer-links">
    <div class="container">
			<div class="row py-5">
				<div class="col-md-4">
					<h5 class="d-inline-block footer-border-bottom pb-2 mb-1">نوشته‌های تازه</h5>
					<ul class="text-small footer-lists">
						<li><a href="3" title="عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه">عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه</a></li>
						<li><a href="3" title="عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه">عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه</a></li>
						<li><a href="3" title="عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه">عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه</a></li>
						<li><a href="3" title="عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه">عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه</a></li>
					</ul>
				</div>
				<div class="col-md-4">
					<h5 class="d-inline-block footer-border-bottom pb-2 mb-1">آخرین تغییرات شرکت‌ها</h5>
					<ul class="text-small footer-lists">
						<li><a href="3" title="عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه">عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه</a></li>
						<li><a href="3" title="عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه">عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه</a></li>
						<li><a href="3" title="عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه">عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه</a></li>
						<li><a href="3" title="عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه">عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه</a></li>
					</ul>
				</div>
				<div class="col-md-4">
					<h5 class="d-inline-block footer-border-bottom pb-2 mb-1">آخرین تغییرات شرکت‌ها</h5>
					<ul class="list-unstyled text-small footer-lists">
						<li><a href="3" title="عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه">عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه</a></li>
						<li><a href="3" title="عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه">عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه</a></li>
						<li><a href="3" title="عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه">عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه</a></li>
						<li><a href="3" title="عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه">عنوان یک نوشته به صورت امتحانی در اینجا میاد که باید طولانی بشه</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="bg-footer w-100">
			<div class="container">
				<div class="row py-5">
					<div class="col-md-3">
						<h5 class="d-inline-block footer-border-bottom border-bottom-blue pb-2 mb-1">سالپا رو می‌شناسی؟</h5>
						<ul class="list-unstyled text-small footer-lists">
							<li><a href="3" title="">درباره سالپا</a></li>
							<li><a href="3" title="">سوالات متداول</a></li>
							<li><a href="3" title="">درباره سالپا</a></li>
							<li><a href="3" title="">درباره سالپا</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						<h5 class="d-inline-block footer-border-bottom border-bottom-blue pb-2 mb-1">می‌خوای تجربه‌ات رو ثبت کنی؟</h5>
						<ul class="list-unstyled text-small footer-lists">
							<li><a href="3" title="">درباره سالپا</a></li>
							<li><a href="3" title="">درباره سالپا</a></li>
							<li><a href="3" title="">درباره سالپا</a></li>
							
						</ul>
					</div>
					<div class="col-md-3">
						<h5 class="d-inline-block footer-border-bottom border-bottom-blue pb-2 mb-1">همراه سالپا یاد بگیریم ...</h5>
						<ul class="list-unstyled text-small footer-lists">
							<li><a href="3" title="">درباره سالپا</a></li>
							<li><a href="3" title="">درباره سالپا</a></li>
							<li><a href="3" title="">یییی</a></li>
						</ul>
					</div>
					<div class="col-md-3">
						لوگو در اینجا
					</div>
				</div>
				<div class="row py-4">
					<div class="col-md-8">
						خلق شده با <i class="fas fa-heart text-danger"></i> در <span class="text-salpa-blue">سال</span><span class="text-salpa-blue-ocean">پا</span>

					</div>
					<div class="col-md-4 text-right">
						<a href="" class="text-muted p-1"><i class="fab fa-instagram fa-2x"></i></a>
						<a href="" class="text-muted p-1"><i class="fab fa-linkedin fa-2x"></i></a>
						<a href="" class="text-muted p-1"><i class="fab fa-pinterest fa-2x"></i></a>
						<a href="" class="text-muted p-1"><i class="fab fa-twitter fa-2x"></i></a>
					</div>
				</div>
			</div>
		</div>
	</footer>
	
	<!-- <footer id="colophon" class="site-footer">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'salpa' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'salpa' ), 'WordPress' );
				?>
			</a>
			<span class="sep"> | </span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'salpa' ), 'salpa', '<a href="http://underscores.me/">Underscores.me</a>' );
				?>
		</div>
	</footer> -->
	
	<!-- #colophon -->
</div><!-- #page -->

<script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery-3.3.1.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/mdb/js/mdb.min.js" type="text/javascript"></script>

<?php wp_footer(); ?>

</body>
</html>
